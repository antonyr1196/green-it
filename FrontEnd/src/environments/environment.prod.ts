export const environment = {
  production: true,
  apiUrl: 'https://antotechsolutions.com/GreenIT-Backend/api/',
  apiFunctions: {
    OrderManagement: {
      ordersList: 'ordersList',
      addOrder: 'addOrder',
      updateOrder: 'updateOrder',
      deleteMultipleOrder: 'deleteMultipleOrder',
    },
  },
};
