import { OrderFormData } from 'src/app/interfaces/order-interface';


/**
* Orders listing grid table structure model
*/
export class orderListingTable {
  displayedColumns: string[] = ['Checkbox', 'Sno', 'Name', 'State', 'Zip', 'Amount', 'Qty', 'Item', 'Action'];
  dataSource: any = [];
  totalRows: number = 0;
  pageSize: number = 10;
  currentPage: number = 0;
  pageSizeOptions: any = ['10', '20'];
  gridLoader: boolean = false;
  tabelFilters: OrderFormData = {Name: '', State: '', Zip: 0, Item: '', Qty: 0, Amount: 0};
}
