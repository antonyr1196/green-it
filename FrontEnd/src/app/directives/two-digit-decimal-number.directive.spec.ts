import { TwoDigitDecimalNumberDirective } from './two-digit-decimal-number.directive';
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
  template: ` <input type="text" appTwoDigitDecimalNumber value="" /> `,
})
class TestTwodigitDecimalComponent {}

describe('Directives TwoDigitDecimalNumberDirective testing', () => {
  let fixture: ComponentFixture<TestTwodigitDecimalComponent>;
  let inputEl: DebugElement;
  const event = new Event('input', {} as any);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TwoDigitDecimalNumberDirective,
        TestTwodigitDecimalComponent,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TestTwodigitDecimalComponent);
    inputEl = fixture.debugElement.query(
      By.directive(TwoDigitDecimalNumberDirective)
    );
  });

  it('Should create an instance for directive', () => {
    const directive = TwoDigitDecimalNumberDirective;
    expect(directive).toBeTruthy();
  });

  it('Should allow only number with 2 decimal places', () => {
    fixture.detectChanges();
    inputEl.nativeElement.value = '10.12';
    let keyDown = new KeyboardEvent('keydown', { key: '5' });
    inputEl.nativeElement.dispatchEvent(keyDown);
    expect(inputEl.nativeElement.value).toBe('10.12');
  });
});
