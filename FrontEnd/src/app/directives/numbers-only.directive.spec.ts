import { NumbersOnlyDirective } from './numbers-only.directive';
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
  template: ` <input type="text" appNumbersOnly value="" /> `,
})
class TestNumbersOnlyComponent {}

describe('Directives NumbersOnlyDirective testing', () => {
  let fixture: ComponentFixture<TestNumbersOnlyComponent>;
  let inputEl: DebugElement;
  const event = new Event('input', {} as any);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NumbersOnlyDirective, TestNumbersOnlyComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestNumbersOnlyComponent);
    inputEl = fixture.debugElement.query(By.directive(NumbersOnlyDirective));
  });

  it('Should create an instance for directive', () => {
    const directive = NumbersOnlyDirective;
    expect(directive).toBeTruthy();
  });

  it('Should allow numbers only', () => {
    fixture.detectChanges();
    inputEl.nativeElement.value = '1a';
    inputEl.nativeElement.dispatchEvent(event);
    expect(inputEl.nativeElement.value).toBe('1');
  });
});
