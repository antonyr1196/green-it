import { StringUppercaseDirective } from './string-uppercase.directive';
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
  template: ` <input type="text" appStringUppercase value="" /> `,
})
class TestStringUppercaseComponent {}

describe('Directives StringUppercaseDirective testing', () => {
  let fixture: ComponentFixture<TestStringUppercaseComponent>;
  let inputEl: DebugElement;
  const event = new Event('input', {} as any);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StringUppercaseDirective, TestStringUppercaseComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestStringUppercaseComponent);
    inputEl = fixture.debugElement.query(
      By.directive(StringUppercaseDirective)
    );
  });

  it('Should create an instance for directive', () => {
    const directive = StringUppercaseDirective;
    expect(directive).toBeTruthy();
  });

  it('Should change the lowercase into uppercase', () => {
    fixture.detectChanges();
    inputEl.nativeElement.value = 'aa';
    inputEl.nativeElement.dispatchEvent(event);
    expect(inputEl.nativeElement.value).toBe('AA');
  });
});
