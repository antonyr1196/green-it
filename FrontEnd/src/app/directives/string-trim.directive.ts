import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appStringTrim]'
})
export class StringTrimDirective {
  constructor(public ref: ElementRef) {}

  @HostListener('input', ['$event']) onInput(event: any) {
    let fieldName = event.target.getAttribute('formcontrolname');
    if(fieldName === "Name"){ // String with single space
      let spaceCounts = event.target.value.split(' ').filter(function (value:string) { return value===""  }).length;
      if(spaceCounts > 1){
        this.ref.nativeElement.value = event.target.value.split(' ').filter(function (value:string) { return value!==""  }).join(' ');
      }
    } else if(fieldName === "Item" || fieldName === "State"){ // String with no spaces
      this.ref.nativeElement.value = event.target.value.split(' ').filter(function (value:string) { return value!==""  }).join('');
    }
  }
}
