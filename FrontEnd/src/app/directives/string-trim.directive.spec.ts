import { StringTrimDirective } from './string-trim.directive';
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';


@Component({
  template: ` <input type="text" formcontrolname="Name" appStringTrim value="" /> `,
})
class TestStringTrimComponent {}

describe('Directives StringTrimDirective testing', () => {
  let fixture: ComponentFixture<TestStringTrimComponent>;
  let inputEl: DebugElement;
  const event = new Event('input', {} as any);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StringTrimDirective, TestStringTrimComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestStringTrimComponent);
    inputEl = fixture.debugElement.query(By.directive(StringTrimDirective));
  });

  it('Should create an instance for directive', () => {
    const directive = StringTrimDirective;
    expect(directive).toBeTruthy();
  });

  it('Should trim the string unwantes whitespaces', () => {
    fixture.detectChanges();
    inputEl.nativeElement.value = 'ab    ba';
    inputEl.nativeElement.dispatchEvent(event);
    expect(inputEl.nativeElement.value).toBe('ab ba');
  });
});
