import { TestBed } from '@angular/core/testing';
import { OrdersService } from './orders.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { environment } from 'src/environments/environment';

describe('OrdersService', () => {
  let service: OrdersService;
  let httpTestingController: HttpTestingController;
  const mockData = {
    status: 'success',
    data: [
      {
        Id: 1,
        Name: 'john',
        State: 'PL',
        Zip: 654798,
        Amount: 1800,
        Qty: 8,
        Item: 'LKSH7698',
      },
      {
        Id: 1,
        Name: 'meenu',
        State: 'KL',
        Zip: 745698,
        Amount: 98000,
        Qty: 8,
        Item: 'LKHG67567498',
      },
    ],
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule],
    });
    service = TestBed.inject(OrdersService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('Should be created the service', () => {
    expect(service).toBeTruthy();
  });

  // Order list api check
  it('Should make a GET request to getOrders() and return all data items', () => {
    service.getOrders().subscribe((res) => {
      expect(res).toEqual(mockData);
      expect(res.data.length).toBe(2);
    });
    let url = environment.apiUrl + environment.apiFunctions.OrderManagement.ordersList;
    const req = httpTestingController.expectOne(url);
    expect(req.request.method).toBe('GET');
    expect(req.cancelled).toBeFalsy();
    expect(req.request.responseType).toEqual('json');
    req.flush(mockData);
    httpTestingController.verify();
  });

  // Order create api check
  it('Should make a POST request with resource data for create item by addOrderData()', () => {
    const createObj = {
      Name: 'jobi',
      State: 'KN',
      Zip: 542598,
      Amount: 10000,
      Qty: 8,
      Item: 'LKFG8798'
    };
    service.addOrderData(createObj).subscribe((res) => {
      expect(res).toBe(createObj);
    });
    let url = environment.apiUrl + environment.apiFunctions.OrderManagement.addOrder;
    const req = httpTestingController.expectOne(url);
    expect(req.request.method).toBe('POST');
    expect(req.cancelled).toBeFalsy();
    expect(req.request.responseType).toEqual('json');
    req.flush(createObj);
    httpTestingController.verify();
  });

  // Order update api check
  it('Should make a POST request with resource data for update item by updateOrderData()', () => {
    const updateObj = {
      Name: 'jobi',
      State: 'KN',
      Zip: 542598,
      Amount: 10000,
      Qty: 8,
      Item: 'LKFG8798',
      Id: 1
    };
    service.updateOrderData(updateObj).subscribe((res) => {
      expect(res).toBe(updateObj);
    });
    let url = environment.apiUrl + environment.apiFunctions.OrderManagement.updateOrder;
    const req = httpTestingController.expectOne(url);
    expect(req.request.method).toBe('POST');
    expect(req.cancelled).toBeFalsy();
    expect(req.request.responseType).toEqual('json');
    req.flush(updateObj);
    httpTestingController.verify();
  });

  // Order delete api check
  it('Should make a POST request with resource data for delete item by deleteOrderData()', () => {
    const item_id = [8,9];
    service.deleteOrderData(item_id).subscribe((res) => {
      expect(res).toBe(item_id);
    });
    let url = environment.apiUrl + environment.apiFunctions.OrderManagement.deleteMultipleOrder;
    const req = httpTestingController.expectOne(url);
    expect(req.request.method).toBe('POST');
    expect(req.cancelled).toBeFalsy();
    expect(req.request.responseType).toEqual('json');
    req.flush(item_id);
    httpTestingController.verify();
  });
});
