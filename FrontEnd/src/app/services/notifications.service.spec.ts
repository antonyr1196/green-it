import { TestBed } from '@angular/core/testing';
import { NotificationsService } from './notifications.service';
import { SharedModule } from 'src/app/shared/shared.module';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

describe('NotificationsService', () => {
  let service: NotificationsService;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule],
    });
    service = TestBed.inject(NotificationsService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
