import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { OrderList, APIResponse } from 'src/app/interfaces/order-interface';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {

  constructor(private http: HttpClient) {}

  /**
   * 
   * function for getting the all orders list
   * @request
   * @returns - success or error response regarding execution & also get the list of orders
   * 
   */
  getOrders(): Observable<OrderList> {
    let url = environment.apiUrl + environment.apiFunctions.OrderManagement.ordersList;
    return this.http.get<OrderList>(url).pipe(catchError(this.ErrorHandler));
  }


  /**
   * 
   * function for adding the single order details
   * @request - send the form-data format object details
   * @returns - success or error response regarding execution
   * 
   */
  addOrderData(orderData: any): Observable<any> {
    let url = environment.apiUrl + environment.apiFunctions.OrderManagement.addOrder;
    return this.http.post<any>(url, orderData).pipe(catchError(this.ErrorHandler));
  }


  /**
   * 
   * function for updating the single order details
   * @request - send the form-data format object details
   * @returns - success or error response regarding execution
   * 
   */
  updateOrderData(orderData: any): Observable<any> {
    const url = environment.apiUrl + environment.apiFunctions.OrderManagement.updateOrder;
    return this.http.post<any>(url, orderData).pipe(catchError(this.ErrorHandler));
  }


  /**
   * 
   * function for deleting the single order & multi orders
   * @request - send the array of order id's in form data
   * @returns - success or error response regarding execution
   * 
   */
  deleteOrderData(orderIds: any): Observable<any> {
    const url = environment.apiUrl + environment.apiFunctions.OrderManagement.deleteMultipleOrder;
    return this.http.post<any>(url, orderIds).pipe(catchError(this.ErrorHandler));
  }


  /**
   * 
   * Common HTTP Errors handler
   * function for error handling
   * @param error
   * @returns
   * 
   */
  ErrorHandler(error: APIResponse) {
    let errorMessage = error.message;
    if(typeof error.error === "undefined"){
      errorMessage = "Server Error. Try Again Later!";
    } else {
      if (error.error.length > 0) { // Validation Errors
        errorMessage = error.error[0];
      }
    }
    return throwError(() => errorMessage);
  }
}
