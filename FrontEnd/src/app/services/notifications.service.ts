import { Injectable } from '@angular/core';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class NotificationsService {
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  displayDuration: number = 1800;

  constructor(private snackBar: MatSnackBar) {}

  // trigger the success notification message
  triggerSuccessNotification(message: string): void {
    this.snackBar.open(message, 'SUCCESS', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: this.displayDuration,
      panelClass: 'success-alert',
    });
  }

  // trigger the error notification message
  triggerErrorNotification(message: string): void {
    this.snackBar.open(message, 'ERROR', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: this.displayDuration,
      panelClass: 'error-alert',
    });
  }
}
