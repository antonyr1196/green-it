import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TwoDigitDecimalNumberDirective } from 'src/app/directives/two-digit-decimal-number.directive';
import { NumbersOnlyDirective } from 'src/app/directives/numbers-only.directive';
import { StringUppercaseDirective } from 'src/app/directives/string-uppercase.directive';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { MatPaginatorModule } from '@angular/material/paginator';
import { OrdersService } from 'src/app/services/orders.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSortModule } from '@angular/material/sort';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ConfirmPopupComponent } from './confirm-popup/confirm-popup.component';
import { HttpErrorCachingInterceptor } from 'src/app/interceptors/http-error-catching.interceptor';
import { GridModule } from '@progress/kendo-angular-grid';
import { StringTrimDirective } from 'src/app/directives/string-trim.directive';

@NgModule({
  declarations: [
    TwoDigitDecimalNumberDirective,
    NumbersOnlyDirective,
    StringUppercaseDirective,
    StringTrimDirective,
    ConfirmPopupComponent,
  ],
  imports: [
    CommonModule,
    MatGridListModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatCardModule,
    MatDialogModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    HttpClientModule,
    FlexLayoutModule,
    NgxSkeletonLoaderModule,
    MatPaginatorModule,
    GridModule,
    MatCheckboxModule,
    MatSortModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule
  ],
  exports: [
    MatGridListModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatCardModule,
    MatDialogModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    HttpClientModule,
    TwoDigitDecimalNumberDirective,
    NumbersOnlyDirective,
    StringUppercaseDirective,
    StringTrimDirective,
    FlexLayoutModule,
    NgxSkeletonLoaderModule,
    MatPaginatorModule,
    GridModule,
    MatCheckboxModule,
    MatSortModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule
  ],
  providers: [
    OrdersService,
    NotificationsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorCachingInterceptor,
      multi: true,
    },
  ],
})
export class SharedModule {}
