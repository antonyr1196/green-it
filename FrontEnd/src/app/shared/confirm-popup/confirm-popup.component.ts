import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PopupDialogData } from 'src/app/interfaces/order-interface';
import { OrdersService } from 'src/app/services/orders.service';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-confirm-popup',
  templateUrl: './confirm-popup.component.html',
  styleUrls: ['./confirm-popup.component.scss'],
})
export class ConfirmPopupComponent implements OnInit {
  loader: boolean = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public DialogData: PopupDialogData,
    public dialogRef: MatDialogRef<ConfirmPopupComponent>,
    private orderService: OrdersService,
    private notification: NotificationsService
  ) {}

  ngOnInit(): void {}


  /**
   * 
   * function for manually close the popup event
   * @request
   * @returns
   * 
   */
  closeDialog() {
    this.dialogRef.disableClose = true;
    this.dialogRef.close();
  }


  /**
   * 
   * function for confirm button click event, collect the order's id & send it for API
   * @request - All the orders id will build as a form-data format & send it
   * @returns - will get the success or error message
   * 
   */
  confirmButton() {
    if (this.DialogData.formMode === 'delete' && this.DialogData.orderIds.length > 0 && !this.loader) {
      this.loader = true;
      const ItemData = new FormData();
      this.DialogData.orderIds.forEach((id: string, index: number) => {
        ItemData.append('Ids['+index+']', id);
      });

      this.orderService.deleteOrderData(ItemData).subscribe((response: any) => {// Valid response
          this.dialogRef.disableClose = true;
          this.dialogRef.close({event: 'close', data: { status: 'success', message: response.message }});
          this.loader = false;
        },(errorMessage: string) => {// Error Handling
          this.notification.triggerErrorNotification(errorMessage);
          this.loader = false;
        }
      );
    }
  }
}
