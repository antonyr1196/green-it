import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfirmPopupComponent } from './confirm-popup.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

describe('ConfirmPopupComponent', () => {
  let component: ConfirmPopupComponent;
  let fixture: ComponentFixture<ConfirmPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ConfirmPopupComponent],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            formData: {
              name: '',
              state: '',
              zip: '',
              amount: '',
              qty: '',
              item: '',
              item_id: '8',
            },
            type: 'Add',
            orderIds: [8, 10],
            displayMessage: 'delete confirmation',
            buttons: {},
          },
        },
        {
          provide: MatDialogRef,
          useValue: {},
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ConfirmPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Should create the confirm popup component', () => {
    expect(component).toBeTruthy();
  });
});
