/**
* Orders listing response handle interface
*/
export interface OrderList {
  status: string;
  data: OrderFormData[];
}


/**
* Orders details structure interface
*/
export interface OrderFormData {
  Id?: number;
  Name: string;
  State: string;
  Zip: number;
  Amount: number;
  Qty: number;
  Item: string;
}


/**
* All popup modal box data passing interface
*/
export interface PopupDialogData {
  formData?: OrderFormData;
  formMode: string;
  orderIds?: any;
  displayMessage?: string;
  buttons?: any;
}


/**
* All API response handle interface
*/
export interface APIResponse {
  status: string;
  message: string;
  error: any;
  code: number;
}
