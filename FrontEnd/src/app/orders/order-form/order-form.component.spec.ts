import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderFormComponent } from './order-form.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('OrderFormComponent', () => {
  let component: OrderFormComponent;
  let fixture: ComponentFixture<OrderFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule, BrowserAnimationsModule],
      declarations: [OrderFormComponent],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            formData: {
              Name: 'Joby',
              State: 'PL',
              Zip: '654798',
              Amount: '10000',
              Qty: 4,
              Item: 'LKJH8798',
              Id: '8',
            },
            type: 'Create',
          },
        },
        { provide: MatDialogRef, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(OrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // validate the zip code
  it('Should validate the zip must be numbers', () => {
    component.myForm.setValue({
      Name: 'Joby',
      State: 'PL',
      Zip: 'test',
      Amount: '10000',
      Qty: 4,
      Item: 'LKJH8798',
    });

    expect(component.myForm.valid).toEqual(false);
  });

  // validate the required fields
  it('Should not allow user to add the item without fill required fields', () => {
    const formData = {
      Name: '',
      State: '',
      Zip: '',
      Amount: '',
      Qty: '',
      Item: '',
    };
    component.myForm.setValue(formData);
    component.submitForm();
    expect(component.myForm.invalid).toEqual(true);
  });
});
