import {Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PopupDialogData } from 'src/app/interfaces/order-interface';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrdersService } from 'src/app/services/orders.service';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.scss'],
})
export class OrderFormComponent implements OnInit {
  myForm: FormGroup;
  formMode: string;
  orderId: any;
  loader: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public DialogInput: PopupDialogData,
    private dialogRef: MatDialogRef<OrderFormComponent>,
    public fb: FormBuilder,
    private orderService: OrdersService,
    private notification: NotificationsService
  ) {}


  /**
   * 
   * function for initalizing & assign values
   * @request
   * @returns
   * 
   */
  ngOnInit(): void {
    this.formMode = this.DialogInput.formMode;
    this.orderId  = this.DialogInput.formData?.Id !== undefined ? this.DialogInput.formData?.Id : 0;
    this.createOrderForm();
  }


  /**
   * 
   * function for creating the order form with reactive form 
   * @request
   * @returns
   * 
   */
  createOrderForm() {
    let ZipCode = this.DialogInput.formData?.Zip===0 ? '' : this.DialogInput.formData?.Zip;
    let Amount  = this.DialogInput.formData?.Amount===0 ? '' : this.DialogInput.formData?.Amount;
    let Qty     = this.DialogInput.formData?.Qty===0 ? '' : this.DialogInput.formData?.Qty;
    this.myForm = this.fb.group({
      Name: [this.DialogInput.formData?.Name, [Validators.required, Validators.pattern('^[a-zA-Z ]*$'), Validators.minLength(3), Validators.maxLength(80)]],
      State: [this.DialogInput.formData?.State, [Validators.required, Validators.pattern('^[a-zA-Z ]*$'), Validators.minLength(2), Validators.maxLength(2)]],
      Zip: [ZipCode, [Validators.required,Validators.pattern('^[0-9]*$'), Validators.minLength(6), Validators.maxLength(6)]],
      Amount: [Amount, [Validators.required, Validators.min(1), Validators.max(99999999999999)]],
      Qty: [Qty, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.min(1), Validators.max(1000)]],
      Item: [this.DialogInput.formData?.Item,[Validators.required, Validators.minLength(3), Validators.maxLength(40)]],
    });
  }


  /**
   * 
   * function for form inline validation error handle
   * @request
   * @returns
   * 
   */
  public errorHandling = (control: string, error: string) => {
    return this.myForm.controls[control].hasError(error);
  };


  /**
   * 
   * function for send the form data to API for create & update
   * @request - sending datas in form-data format
   * @returns - getting the response regarding success or fail
   * 
   */
  submitForm() {
    if (this.myForm.valid && !this.loader) {
      // Payload creation for Create & Update Order
      this.loader = true;
      const orderData = new FormData();
      orderData.append('Name', this.myForm.value.Name);
      orderData.append('State', this.myForm.value.State);
      orderData.append('Zip', this.myForm.value.Zip);
      orderData.append('Qty', parseInt(this.myForm.value.Qty).toString());
      orderData.append('Amount', parseFloat(this.myForm.value.Amount).toString());
      orderData.append('Item', this.myForm.value.Item);

      if (this.DialogInput.formData?.Id !== 0) { // For Update
        orderData.append('Id', this.orderId);
        this.orderService.updateOrderData(orderData).subscribe((response: any) => { // Valid response
          this.dialogRef.disableClose = true;
          this.dialogRef.close({event: 'close',data: { status: 'success', message: response.message }});
          this.loader = false;
        },(errorMessage: string) => { // Error Handling
          this.notification.triggerErrorNotification(errorMessage);
          this.loader = false;
        });
      } else { // For Create
        this.orderService.addOrderData(orderData).subscribe((response: any) => { // Valid response
          this.dialogRef.disableClose = true;
          this.dialogRef.close({event: 'close',data: { status: 'success', message: response.message }});
          this.loader = false;
        },(errorMessage: string) => { // Error Handling
          this.notification.triggerErrorNotification(errorMessage);
          this.loader = false;
        });
      }
    }
  }


  /**
   * 
   * function for manually close the popup event
   * @request
   * @returns
   * 
   */
  closePopup(){
    this.dialogRef.disableClose = true;
    this.dialogRef.close();
  }
}
