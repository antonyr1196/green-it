import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { orderListingTable } from 'src/app/models/orders.model';
import { OrdersService } from 'src/app/services/orders.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmPopupComponent } from 'src/app/shared/confirm-popup/confirm-popup.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OrderFormComponent } from 'src/app/orders/order-form/order-form.component';
import { OrderFormData, OrderList } from 'src/app/interfaces/order-interface';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ordersGridTable = new orderListingTable();
  tempGridData: OrderFormData;
  formData: OrderFormData;
  formdialogRef: any;
  confirmdialogRef: any;
  checkedAll: boolean = false;
  filterOpened: boolean = false;
  selectedOrders: any = [];

  constructor(
    private orderService: OrdersService,
    public dialog: MatDialog,
    private notification: NotificationsService
  ) {}


  /**
   * 
   * function for initial loading with assign values
   * @request
   * @returns
   * 
   */
  ngAfterViewInit() {
    this.checkedAll = false;
    this.selectedOrders = [];
    this.loadOrderLists();
  }


  /**
   * 
   * function for load all orders from API & load into Meterial Grid table
   * @request
   * @returns
   * 
   */
  loadOrderLists() {
    this.ordersGridTable.gridLoader = true;
    this.orderService.getOrders().subscribe((response: OrderList) => { // Valid response
      this.ordersGridTable.dataSource = new MatTableDataSource(response.data);
      this.tempGridData = JSON.parse(JSON.stringify(response.data));
      this.ordersGridTable.dataSource.sort = this.sort;
      this.ordersGridTable.dataSource.paginator = this.paginator;
      this.ordersGridTable.gridLoader = false;
    },(errorMessage: string) => { // Error Handling
        this.notification.triggerErrorNotification(errorMessage);
      }
    );
  }


  /**
   * 
   * function for assign default values & send it for create form
   * @request
   * @returns
   * 
   */
  addOrder() {
    this.formData = {Id: 0, Name: '', State: '', Zip: 0, Amount: 0, Qty: 0, Item: ''};
    this.openForm('Create');
  }


  /**
   * 
   * function for assign existing values & send it for update form
   * @request
   * @returns
   * 
   */
  updateOrder(elementData: OrderFormData) {
    this.formData = {
      Id: elementData.Id, 
      Name: elementData.Name, 
      State: elementData.State, 
      Zip: elementData.Zip, 
      Amount: elementData.Amount, 
      Qty: elementData.Qty, 
      Item: elementData.Item
    };
    this.openForm('Update');
  }


  /**
   * 
   * function for open the popup dialog & send form datas to component for create & update order
   * @request
   * @returns
   * 
   */
  openForm(formMode: string) {
    this.formdialogRef = this.dialog.open(OrderFormComponent, {
      width: '600px',
      disableClose: true,
      data: {
        formData: this.formData,
        formMode: formMode,
      },
    });

    this.formdialogRef.afterClosed().subscribe((result: any) => {
      if (typeof result.data !== 'undefined') {
        this.notification.triggerSuccessNotification(result.data.message);
        this.ngAfterViewInit();
      }
    });
  }


  /**
   * 
   * function for show & hide the Advanced filter section
   * @request
   * @returns
   * 
   */
  openFilterPopup() {
    if (this.filterOpened) {
      this.filterOpened = false;
    } else {
      this.filterOpened = true;
    }
  }


  /**
   * 
   * function for check all check box click event
   * @request
   * @returns
   * 
   */
  checkAllOrders(event: any) {
    this.checkedAll = event.checked;
    if (event.checked) {
      this.selectedOrders = this.ordersGridTable.dataSource._data._value.map((order: any) => order.Id);
    } else {
      this.selectedOrders = [];
    }
  }


  /**
   * 
   * function for select particular checkbox click event
   * @request
   * @returns
   * 
   */
  onCheckOrder(element: any) {
    const orderIndex = this.selectedOrders.indexOf(element.Id);
    if (orderIndex > -1) {
      this.selectedOrders.splice(orderIndex, 1);
    } else {
      this.selectedOrders.push(element.Id);
    }

    // check if all checkboxes selected or not
    if (this.selectedOrders.length === this.ordersGridTable.dataSource._data._value.length || this.selectedOrders.length > 0) {
      this.checkedAll = true;
    } else if (this.selectedOrders.length === 0) {
      this.checkedAll = false;
    }
  }


  /**
   * 
   * function for delete confirmation popup open, WIll send multiple & single order id's to the component
   * @request
   * @returns
   * 
   */
  deleteOrders(multiDelete: boolean, orderId = null) {
    let OrderIds = multiDelete ? this.selectedOrders : [orderId];
    let message = 'Are you sure do you want to delete the ' + OrderIds.length + ' Orders!';
    this.confirmdialogRef = this.dialog.open(ConfirmPopupComponent, {
      width: '458px',
      data: {
        formMode: 'delete',
        orderIds: OrderIds,
        displayMessage: message,
        buttons: {ok: 'Delete', cancel: 'Cancel'}
      },
    });

    this.confirmdialogRef.afterClosed().subscribe((result: any) => {
      if (typeof result.data !== 'undefined') {
        this.notification.triggerSuccessNotification(result.data.message);
        this.ngAfterViewInit();
      }
    });
  }


  /**
   * 
   * function for advanced filter response handle & load the results into the grid
   * @request
   * @returns
   * 
   */
  advancedFilterResponseHandle(event: any) {
    this.ordersGridTable.gridLoader = true;
    this.ordersGridTable.dataSource = [];
    this.ordersGridTable.tabelFilters = event.tabelFilters;
    let gridData = JSON.parse(JSON.stringify(this.tempGridData));
    let filterData = gridData;

    // Order name field filter
    if (this.ordersGridTable.tabelFilters.Name !== '') {
      filterData = filterData.filter((data: any) => {
        return (data.Name.toLowerCase().indexOf(this.ordersGridTable.tabelFilters.Name.toLowerCase()) > -1);
      });
    }

    // Order code field filter
    if (this.ordersGridTable.tabelFilters.Item !== '') {
      filterData = filterData.filter((data: any) => {
        return (data.Item.toLowerCase().indexOf(this.ordersGridTable.tabelFilters.Item.toLowerCase()) > -1);
      });
    }

    // Order zip code field filter
    if (this.ordersGridTable.tabelFilters.Zip !== 0) {
      filterData = filterData.filter((data: any) => {
        return (data.Zip.toLowerCase().indexOf(this.ordersGridTable.tabelFilters.Zip.toString().toLowerCase()) > -1);
      });
    }

    // Order state code field filter
    if (this.ordersGridTable.tabelFilters.State !== '') {
      filterData = filterData.filter((data: any) => {
        return (data.State.toLowerCase().indexOf(this.ordersGridTable.tabelFilters.State.toLowerCase()) > -1);
      });
    }

    setTimeout(() => {
      if (filterData.length > 0) {
        this.ordersGridTable.dataSource = new MatTableDataSource(filterData);
        this.ordersGridTable.dataSource.sort = this.sort;
        this.ordersGridTable.dataSource.paginator = this.paginator;
      } else {
        this.ordersGridTable.dataSource = [];
      }
      this.ordersGridTable.gridLoader = false;
    }, 200);
  }
}
