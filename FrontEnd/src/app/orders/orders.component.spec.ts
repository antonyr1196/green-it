import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OrdersComponent } from './orders.component';
import { of } from 'rxjs';
import { OrdersService } from 'src/app/services/orders.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('OrdersComponent', () => {
  let component: OrdersComponent;
  let fixture: ComponentFixture<OrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule, BrowserAnimationsModule],
      declarations: [OrdersComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(OrdersComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  describe('ngAfterViewInit', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    // all the listing data check
    it('should get all the items from loadOrderLists', () => {
      const service = fixture.debugElement.injector.get(OrdersService);
      let sampleResponse: any = [
        {
          Id: '2',
          Name: 'manu',
          State: 'KL',
          Zip: '659898',
          Amount: '8900',
          Qty: '10',
          Item: 'LKSH9808',
        }
      ];
      let expectedResponse: any = [
        {
          Id: '2',
          Name: 'manu',
          State: 'KL',
          Zip: '659898',
          Amount: '8900',
          Qty: '10',
          Item: 'LKSH9808',
        }
      ];
      //spyOn(service, 'getOrders').and.returnValue(of(sampleResponse));
      component.loadOrderLists();
      expect(sampleResponse).toEqual(expectedResponse);
      expect(component).toBeTruthy();
    });
  });
});
