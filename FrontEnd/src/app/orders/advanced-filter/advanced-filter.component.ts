import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-advanced-filter',
  templateUrl: './advanced-filter.component.html',
  styleUrls: ['./advanced-filter.component.scss'],
})
export class AdvancedFilterComponent implements OnInit {
  @Input() tabelFilters: any;
  @Output() filterResponse: EventEmitter<any> = new EventEmitter();
  applied: boolean = false;
  canceled: boolean = false;
  constructor() {}

  ngOnInit(): void {
  }

  /**
   * 
   * function for apply the filters with some values
   * @request
   * @returns
   * 
   */
  applyFilter() {
    this.applied = true;
    this.triggerResponse();
  }


  /**
   * 
   * function for clear all the previous filters from the search
   * @request
   * @returns
   * 
   */
  clearFilter() {
    this.canceled = true;
    this.tabelFilters.Name = '';
    this.tabelFilters.Item = '';
    this.tabelFilters.Zip = '';
    this.tabelFilters.State = '';
    this.triggerResponse();
  }


  /**
   * 
   * function for trigger to parent component with filter values
   * @request
   * @returns
   * 
   */
  triggerResponse() {
    this.filterResponse.emit({
      tabelFilters: this.tabelFilters,
      canceled: this.canceled,
      applied: this.applied,
    });
  }
}
