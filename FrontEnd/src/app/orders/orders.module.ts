import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './orders.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { OrderFormComponent } from './order-form/order-form.component';
import { AdvancedFilterComponent } from './advanced-filter/advanced-filter.component';

@NgModule({
  declarations: [OrdersComponent, OrderFormComponent, AdvancedFilterComponent],
  imports: [CommonModule, OrdersRoutingModule, SharedModule],
})
export class OrdersModule {}
