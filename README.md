## Green-IT Application Challenge

<p align="center">
Green-IT Order Management Software
</p>

This project will be a fully functional web application that will display the given data from a csv file, and allow full editing capabilties of the data. It's perfoming the CURD operations on CSV file.

There are 2 type of application is there. One for frontend developed in Angular Framework & other one is Backend for API's developed in PHP Laravel Framework. In this application Data was collected from the frontend side and stored on Csv file using the Backend API's.

In this application we are provided the advanced filter functionality. So we can able to filter the orders details based on the requirement. The table sorting feature also available on this.

The multi delete option also available on this application. So we can able to delete multiple order's in a single shot using the checkbox selection.

## Application Screenshots
![Screenshot_main-screen](https://antotechsolutions.com/project-images/main-screen.jpg)
![Screenshot_create](https://antotechsolutions.com/project-images/create.jpg)
![Screenshot_update](https://antotechsolutions.com/project-images/update.jpg)
![Screenshot_delete](https://antotechsolutions.com/project-images/delete.jpg)
![Screenshot_search](https://antotechsolutions.com/project-images/advanced-search.jpg)


## Application Used Framework & Language Details

### Backend
- Language : PHP
- PHP Version : v7.4.29
- PHP Framework : Laravel
- Laravel Version : v8.83.27

### Frontend
- Language : Typescript
- Framework Name : Angular
- Framework Version : v14.1.3
- UI : Angular Meterial

## Application Installation & configuration steps for run

### Angular Application:
1. Clone or donwload the repository.
2. Run `npm install` Inside the project folder.
3. Go to environments/environment.ts file & configure the local Backend application API url path.
4. Run `ng serve`.
5. Visit **http://localhost:4200** for saw the running application.

### PHP Application:
1. Clone or donwload the repository.
2. Install the composer globally.
3. Open the Xampp or any local server & run the apache.
4. Run `composer update` Inside the project folder.
5. Visit **http://localhost/api** for saw the running API's.

## Application Unit Testing

### Angular Unit Test :
The Angular CLI takes care of Jasmine and Karma configuration for you. It constructs the full configuration in memory, based on options specified in the angular.json file.

1. go to the project directory and run `ng test`.

Screenshot for Angular unit test case
![Screenshot_angular_test](https://antotechsolutions.com/project-images/angular-unit-test.jpg)

### Laravel Unit Test :
aravel is built with testing in mind. In fact, support for testing with PHPUnit is included out of the box and a phpunit.xml file is already set up for your application. The framework also ships with convenient helper methods that allow you to expressively test your applications.

1. go to the project directory and run `php artisan test`.

Screenshot for Laravel unit test case
![Screenshot_laravel_test](https://antotechsolutions.com/project-images/laravel-unit-test.jpg)
