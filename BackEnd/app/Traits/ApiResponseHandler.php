<?php

namespace App\Traits;

use Illuminate\Http\Response as Response;
use Illuminate\Http\JsonResponse;

trait ApiResponseHandler
{

  /*
   * Build a success response
   * @param string|array $data
   * @param int $code
   * @return Illuminate\Http\Response
   */
  public function successResponse($message, $code = Response::HTTP_OK, $data = []):JsonResponse
  {
    return response()->json(['status' => 'success', 'message' => $message,'data' => $data], $code)->header('Content-type', 'application/json');
  }


  /*
   * Build a error response
   * @param string $message
   * @param int $code
   * @return Illuminate\Http\JsonResponse
   */
  public function errorResponse($message, $code, $error = [])
  {
    return response()->json(['status' => 'error', 'message' => $message, 'error' => $error, 'code' => $code], $code);
  }
}