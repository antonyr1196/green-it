<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use League\Csv\Reader;
use Exception;

class OrderData extends Model
{
    use HasFactory;

    

    /********************************* Common Centralized Funtions Start Here *************************************/

    /*
     *  Sending the commom csv headers
     *  
     *  @return string
     *
     */
    public static function getHeaders(){
        return ['Id', 'Name','State','Zip','Amount','Qty','Item'];
    }


    /*
     *  Sending the commom csv file located path
     *  
     *  @return string
     *
     */
    public static function getFilePath(){
        return base_path('public\OrderManagement.csv');
    }

    
    /*
     *  Checking the csv in specific path it's available or not
     *  
     *  @return boolean
     *
     */
    public static function checkFileExists(){
        if(!file_exists(self::getFilePath())){
            return false;
        }
        return true;
    }


    /*
     *  It's reading the data from csv file & write, update the data's into the csv file
     *  
     *  @return boolean while writing
     *  @return array while reading
     *
     */
    public static function fileHanlde($type, $orderData = []){
        if($type == 1){ // Read file
            $Order_File     = fopen(self::getFilePath(), "r+");
            $ordersList     = [];
            $header         = [];
            $datas          = [];
            while (($rowData = fgetcsv($Order_File, 1000, ",")) !== FALSE) {  
                if(empty($header)){
                    $header = $rowData;
                } else {
                    $ordersList[] = array_combine($header, $rowData); 
                }  
            }
            fclose($Order_File);
            return $ordersList;
        } else { // Write data into file
            $Order_File  = fopen(self::getFilePath(), "w");
            foreach ($orderData as $fields) {
                fputcsv($Order_File, $fields);
            }
            fclose($Order_File);
            return true;
        }
    }


    /*
     *  It's checking like if the Item code & Order id already exists or not
     *  
     *  @return boolean
     *
     */
    public static function checkUnique($item, $field, $id=null){
        $responseStatus = false;
        try{
            // get the order details
            $ordersDataResponse     = self::getData();
            if($ordersDataResponse['status'] == 0){ // Exception triggred
                $responseStatus = true;
            } else {
                $ordersList    = $ordersDataResponse['orders'];
                $existItem     = array_values(array_filter($ordersList, function ($order) use ($item, $field) {
                    return ($order[$field] == $item);
                }));

                if(count($existItem) > 0){ // Field value already exist
                    if($field == "Item"){ // Order item field checking
                        if($id == null || $existItem[0]['Id'] != $id){
                            $responseStatus = true;
                        }
                    } else { // Remaining fields checking
                        $responseStatus = true;
                    }
                }
            }
        } catch (Exception $e) { // Exception Handle
            report($e);
            $responseStatus = true;
        }
        return $responseStatus;
    }


    /*
     *  It's check & return the next primary key of the record
     *  
     *  @return number
     *
     */
    public static function getPrimaryKey(){
        $primaryKey  = 0;
        try{
            // get the order details
            $ordersDataResponse     = self::getData();
            if($ordersDataResponse['status'] == 0){ // Exception triggred
                $primaryKey = 0;
            } else {
                if(count($ordersDataResponse['orders']) > 0){
                    $lastData    = end($ordersDataResponse['orders']);
                    $newId       = $lastData['Id']+1;
                    $primaryKey  = $newId;
                } else {
                    $primaryKey = 0;
                }
            }
        } catch (Exception $e) { // Exception Handle
            report($e);
            $primaryKey = 0;
        }
        return $primaryKey;
    }


    /********************************* Common Centralized Funtions Ends Here *************************************/


    /*
     *
     *
     * 
     *
     *  Get Order list values and send it back function
     *  
     *  @return array
     *
     */
    public static function getData(){
        try {
            return array("status" => 1, "message" => "success", "orders"=>self::fileHanlde(1));
        } catch (Exception $e) { // Exception Handle
            report($e);
            return array("status" => 0, 'message' => 'Internal server error. Try again.');
        }
    }


    /*
     *  Add new order details into the csv file
     *  
     *  @return boolean
     *
     */
    public static function insertData($newData){
        try{
            // get the order details
            $ordersDataResponse     = self::getData();
            if($ordersDataResponse['status'] == 0){ // Exception triggred
                return false;
            } else {
                $ordersList    = array_values($ordersDataResponse['orders']);
                $insertData[]  = self::getHeaders();
                foreach ($ordersList as $field) {
                    $insertData[] = array_values($field);
                }
                $insertData[] = $newData;
                return self::fileHanlde(2, $insertData);
            }
        } catch (Exception $e) { // Exception Handle
            report($e);
            return false;
        }
    }


    /*
     *  Update existing order details into the csv file
     *  
     *  @return boolean
     *
     */
    public static function updateData($updatedData){
        try{
            // get the order details
            $ordersDataResponse     = self::getData();
            if($ordersDataResponse['status'] == 0){ // Exception triggred
                return false;
            } else {
                $ordersList    = array_values($ordersDataResponse['orders']);
                $updateData[]  = self::getHeaders();
                foreach ($ordersList as $field) {
                    if($field['Id'] == $updatedData[0]){
                        $updateData[] = $updatedData;
                    } else {
                        $updateData[] = array_values($field);
                    }
                }
                return self::fileHanlde(2, $updateData);
            }
        } catch (Exception $e) { // Exception Handle
            report($e);
            return false;
        }
    }


    /*
     *  Delete existing order details from the csv file
     *  
     *  @return boolean
     *
     */
    public static function deleteData($orderIds){
        try{
            // get the order details
            $ordersDataResponse     = self::getData();
            if($ordersDataResponse['status'] == 0){ // Exception triggred
                return false;
            } else {
                $ordersList     = array_values($ordersDataResponse['orders']);
                $deletedData[]  = self::getHeaders();
                foreach ($ordersList as $field) {
                    if(!in_array($field['Id'], $orderIds)){
                        $deletedData[] = array_values($field);
                    }
                }
                return self::fileHanlde(2, $deletedData);
            }
        } catch (Exception $e) { // Exception Handle
            report($e);
            return false;
        }
    }

}