<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\OrderData;
use App\Traits\ApiResponseHandler;
use Validator;

class OrdersController extends Controller
{
    use ApiResponseHandler;


    /*
     *  Function for sending all the orders list
     *  
     *  @method GET
     *  @response JSON Array
     *
     */
    public function List(Request $request)
    {
        // If the file exist or not check
        if(!OrderData::checkFileExists()){
            return $this->errorResponse('File is not available.', 404);
        }

        // read the data from file
        $responseData      = OrderData::getData();
        if($responseData['status'] == 1){ // success response
            return $this->successResponse($responseData['message'], 200, $responseData['orders']);
        } else { // error response
            return $this->errorResponse($responseData['message'], 500);
        }
    }


    /*
     *  Function for adding the single order
     *  
     *  @method POST
     *  @request array (receive all datas in form-data format)
     *  @response JSON Array
     *
     */
    public function Add(Request $request)
    {
        // If the file exist or not check
        if(!OrderData::checkFileExists()){
            return $this->errorResponse('File is not available.', 404);
        }

        // validate the requested params
        $validateResponse = $this->Validation($request, 'create');
        if($validateResponse['status'] == 0){ // validation faild
            return $this->errorResponse("Validation Errors", 401, $validateResponse['errors']);
        } else { // validation success
            // add the order data & send back the response
            $primaryId          = OrderData::getPrimaryKey();
            $orderDetails       = [$primaryId, $request->Name, $request->State, $request->Zip, $request->Amount, $request->Qty, $request->Item];
            
            if(OrderData::insertData($orderDetails)){
                return $this->successResponse('New order added successfully.', 200);
            } else {
                return $this->errorResponse('Sorry order is not added', 500);
            }
        }
    }



    /*
     *  Function for updating the single order
     *  
     *  @method PUT
     *  @request array (receive all datas in form-data format)
     *  @response JSON Array
     *
     */
    public function Update(Request $request)
    {
        // If the file exist or not check
        if(!OrderData::checkFileExists()){
            return $this->errorResponse('File is not available.', 404);
        }

        // validate the requested params
        $validateResponse = $this->Validation($request, 'update');
        if($validateResponse['status'] == 0){ // validation faild
            return $this->errorResponse("Validation Errors", 401, $validateResponse['errors']);
        } else { // validation success
            // update the order data & send back the response
            $orderDetails  = [$request->Id, trim($request->Name), $request->State, $request->Zip, $request->Amount, $request->Qty, $request->Item];
            if(OrderData::updateData($orderDetails)){
                return $this->successResponse('Order details updated successfully.', 200);
            } else {
                return $this->errorResponse('Sorry order is not updated', 500);
            }
        }
    }



    /*
     *  Function for deleting the single & multiple order
     *  
     *  @method DELETE
     *  @request array (list of order primary id will get)
     *  @response JSON Array
     *
     */
    public function DeleteMultiple(Request $request)
    {
        // If the file exist or not check
        if(!OrderData::checkFileExists()){
            return $this->errorResponse('File is not available.', 404);
        }

        if(!isset($request->Ids) || !is_array($request->Ids)){
            return $this->errorResponse("Validation Errors", 401, ['Collection of order ids required.']);
        } else {
            if(OrderData::deleteData($request->Ids)){
                return $this->successResponse('Order details deleted successfully.', 200);
            } else {
                return $this->errorResponse('Sorry order is not deleted', 500);
            }
        }
    }



    /*
     *  Commom validation for add & update api requests
     *  
     *  @params $request array, $mode string
     *  @response JSON Array
     *
     */
    public function Validation(Request $request, $mode){
        $validator = Validator::make($request->all(), [
            'Id'      => 'nullable',
            'Name'    => 'required|string|min:3|max:80',
            'State'   => 'required|string|min:2|max:2',
            'Zip'     => 'required|numeric|digits:6',
            'Amount'  => 'required|numeric|between:1,99999999999999.99',
            'Qty'     => 'required|numeric|between:1,1000',
            'Item'    => 'required|alpha_num','min:3','max:40',
        ]);

        if(!$validator->passes()) {
            return array("status" => 0, "errors" => $validator->errors()->all());
        } else {
            $validatorErrors = [];
            if($mode == "update" && $request->Id == ""){
                array_push($validatorErrors,'Order id is required.');
            }
            if($mode == "update" && $request->Id != "" && !OrderData::checkUnique($request->Id, "Id")){
                array_push($validatorErrors,'The order id is invalid.');
            }
            if($mode == "update" && $request->Id != "" && OrderData::checkUnique($request->Item, "Item", $request->Id)){
                array_push($validatorErrors,'The Item is already exists.');
            }
            if($mode == "create" && OrderData::checkUnique($request->Item, "Item")){
                array_push($validatorErrors,'The Item is already exists.');
            }


            if(count($validatorErrors) > 0){
                return array("status" => 0, "errors" => $validatorErrors);
            } else {
                return array("status" => 1, "errors" => []);
            }
        }
    }
}