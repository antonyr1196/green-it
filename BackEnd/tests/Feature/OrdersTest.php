<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\OrderData;

class OrdersTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }



    /**
     * A functional fail test for creating a order test example without proper data.
     *
     * @return json data
     */
    public function test_create_order_fail()
    {
        $response = $this->postJson('/api/addOrder', [
            'Name' => 'prince',
            'State' => 'KA',
            'Zip'=>876598,
            'Amount'=>18000,
            'Qty'=>8,
            'Item'=>''
        ]);
        $response->assertStatus(401)->assertJson(['status' => 'error']);
    }



    /**
     * A functional success test for creating a order test example without proper data.
     *
     * @return json data
     */
    public function test_create_order_success()
    {
        $strings        = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $randomString   = substr(str_shuffle($strings),0, 8);
        $response = $this->postJson('/api/addOrder', [
            'Name' => 'prince',
            'State' => 'KA',
            'Zip'=>876598,
            'Amount'=>18000,
            'Qty'=>8,
            'Item'=>$randomString
        ]);
        $response->assertStatus(200)->assertJson(['status' => 'success']);
    }



    /**
     * A functional fail test for updating a order test example without proper data.
     *
     * @return json data
     */
    public function test_update_order_fail()
    {
        $strings        = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $randomString   = substr(str_shuffle($strings),0, 8);
        $response = $this->postJson('/api/updateOrder', [
            'Name' => 'prince',
            'State' => 'KA',
            'Zip'=>876598,
            'Amount'=>18000,
            'Qty'=>8,
            'Item'=>$randomString
        ]);
        $response->assertStatus(401)->assertJson(['status' => 'error']);
    }



    /**
     * A functional success test for updating a order test example without proper data.
     *
     * @return json data
     */
    public function test_update_order_success()
    {
        $strings        = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $randomString   = substr(str_shuffle($strings),0, 8);
        $responseData   = OrderData::getData();
        $lastRecord     = end($responseData['orders']);
        $lastId         = $lastRecord['Id'];

        $response = $this->postJson('/api/updateOrder', [
            'Id' => $lastId,
            'Name' => 'prince kh',
            'State' => 'KA',
            'Zip'=>876598,
            'Amount'=>18000,
            'Qty'=>8,
            'Item'=>$randomString
        ]);
        $response->assertStatus(200)->assertJson(['status' => 'success']);
    }


    /**
     * A functional success test for listing a order with proper data.
     *
     * @return json data
     */
    public function test_order_list()
    {
        $response = $this->getJson('/api/ordersList');
        $response->assertStatus(200)->assertJson(['status' => 'success']);
    }



    /**
     * A functional success test for multiple deleting a order test example without proper data.
     *
     * @return json data
     */
    public function test_delete_multiple_order_success()
    {
        $response = $this->postJson('/api/deleteMultipleOrder',[
            "Ids" => [10,20]
        ]);
        $response->assertStatus(200)->assertJson(['status' => 'success']);
    }



    /**
     * A functional fail test for multiple deleting a order test example without proper data.
     *
     * @return json data
     */
    public function test_delete_multiple_order_fail()
    {
        $response = $this->postJson('/api/deleteMultipleOrder',[]);
        $response->assertStatus(401)->assertJson(['status' => 'error']);
    }
}
