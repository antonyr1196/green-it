<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrdersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/ordersList', [OrdersController::class,'List'])->middleware("cors");
Route::post('/addOrder', [OrdersController::class,'Add'])->middleware("cors");
Route::post('/updateOrder', [OrdersController::class,'Update'])->middleware("cors");
Route::post('/deleteMultipleOrder', [OrdersController::class,'DeleteMultiple'])->middleware("cors");