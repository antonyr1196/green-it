<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HCL Mechine Test For Item Management</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/bootstrap.min.css') }}" />
    <style>
    .download-csv-btn {
        position: absolute;
        right: 16px;
    }

    .title {
        font-size: 18px;
        font-weight: 500;
    }

    .card {
        margin: 20px;
    }
    </style>
    <!-- Scripts -->
    <script src="{{ asset('public/assets/js/jquery.slim.min.js') }}" defer></script>
    <script src="{{ asset('public/assets/js/popper.min.js') }}" defer></script>
    <script src="{{ asset('public/assets/js/bootstrap.bundle.min.js') }}" defer></script>
</head>

<body class="antialiased">
    <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link title" href="#">Welcome to Green IT Sample Test Application</a>
            </li>
        </ul>
        <!-- <a type="button" href="{{ url('/download') }}" class="btn btn-success download-csv-btn">Download Latest DB (Csv File)</a> -->
    </nav>

    <!-- BackEnd Details -->
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Backend Application Details</h4>
            <ul class="list-group">
                <li class="list-group-item"><b>Language : </b> PHP</li>
                <li class="list-group-item"><b>PHP Version : </b> v7.4.29</li>
                <li class="list-group-item"><b>PHP Framework : </b> Laravel</li>
                <li class="list-group-item"><b>Framework Version : </b> v8.83.27</li>
                <li class="list-group-item"><b>Application Live Deployed Link : </b> <a
                        href="https://antotechsolutions.com/GteenIT-Backend/" target="_blank">Click here</a></li>
            </ul>
        </div>
    </div>

    <!-- FrontEnd Details -->
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Frontend Application Details</h4>
            <ul class="list-group">
                <li class="list-group-item"><b>Language : </b> Typescript</li>
                <li class="list-group-item"><b>PHP Version : </b> v4.7.2</li>
                <li class="list-group-item"><b>Framework Name : </b> Angular</li>
                <li class="list-group-item"><b>Framework Version : </b> v14.1.3</li>
                <li class="list-group-item"><b>Application Live Deployed Link : </b> <a
                        href="https://antotechsolutions.com/GreenIT-Frontend/" target="_blank">Click here</a></li>
            </ul>
        </div>
    </div>
</body>

</html>